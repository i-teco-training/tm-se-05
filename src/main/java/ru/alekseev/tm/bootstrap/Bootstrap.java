package ru.alekseev.tm.bootstrap;

import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.command.project.*;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.command.system.ExitCommand;
import ru.alekseev.tm.command.system.HelpCommand;
import ru.alekseev.tm.command.task.*;
import ru.alekseev.tm.repository.ProjectRepository;
import ru.alekseev.tm.repository.TaskRepository;
import ru.alekseev.tm.service.ProjectService;
import ru.alekseev.tm.service.TaskService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Bootstrap implements ServiceLocator {
    private ProjectRepository projectRepository = new ProjectRepository();
    private ProjectService projectService = new ProjectService(projectRepository);
    private TaskRepository taskRepository = new TaskRepository();
    private TaskService taskService = new TaskService(taskRepository);

    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final Map<String, AbstractCommand> commands = new HashMap<>();

    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    @Override
    public ProjectService getProjectService() {
        return projectService;
    }

    @Override
    public TaskService getTaskService() {
        return taskService;
    }

    public void registry(AbstractCommand command) {
        String commandName = command.getName();
        commands.put(commandName, command);
    }

    public void init() throws Exception { //надо поймать Exception(?)
        registry(new HelpCommand(this));
        registry(new ExitCommand(this));
        registry(new ProjectAddCommand(this));
        registry(new ProjectClearCommand(this));
        registry(new ProjectDeleteCommand(this));
        registry(new ProjectListCommand(this));
        registry(new ProjectUpdateCommand(this));
        registry(new TaskAddCommand(this));
        registry(new TaskClearCommand(this));
        registry(new TaskDeleteCommand(this));
        registry(new TaskListCommand(this));
        registry(new TaskUpdateCommand(this));

        System.out.println("*** WELCOME TO TASK MANAGER ***");

        while (true) {
            String commandName = reader.readLine();// оперировать мапой ок? или надо оперировать листом?
            if (!commands.containsKey(commandName)) {
                commandName = "help";
            }
            commands.get(commandName).execute();
            System.out.println();
        }
    }
}