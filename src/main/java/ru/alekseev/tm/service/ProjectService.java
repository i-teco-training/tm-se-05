package ru.alekseev.tm.service;

import ru.alekseev.tm.entity.Project;
import ru.alekseev.tm.repository.ProjectRepository;

import java.util.List;

public class ProjectService {

    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public void addProject(String name) {
        if (name == null || name.isEmpty()) return;
        Project project = new Project();
        project.setName(name);
        this.projectRepository.persist(project);
    }

    public List<Project> showProjects() {
        List<Project> list = this.projectRepository.findAll();
        return list;
    }

    public void updateProject(String projectId, String name) {
        if (projectId == null || projectId.isEmpty() || name == null || name.isEmpty()) return;
        Project project = new Project();
        project.setName(name);
        project.setId(projectId);
        this.projectRepository.merge(project);
    }

    public void deleteProject(String projectId) {
        Project projectForExistenceChecking = this.projectRepository.findOne(projectId);
        if (projectId == null || projectId.isEmpty() || projectForExistenceChecking == null) return;
        //про projectForExistenceChecking ok?
        this.projectRepository.remove(projectId);
    }

    public void clearProjects() {
        this.projectRepository.removeAll();
    }
}
