package ru.alekseev.tm.service;

import ru.alekseev.tm.entity.Task;
import ru.alekseev.tm.repository.TaskRepository;

import java.util.List;

public class TaskService {
    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public void addTask(String name, String projectId) {
        if (name == null || name.isEmpty()) return;
        Task task = new Task();
        task.setName(name);
        task.setProjectId(projectId);
        this.taskRepository.persist(task);
    }

    public List<Task> showTasks() {
        List<Task> list = this.taskRepository.findAll();
        return list;
    }

    public void updateTask(String taskId, String name) {
        if (taskId == null || taskId.isEmpty() || name == null || name.isEmpty()) return;
        Task oldTask = this.taskRepository.findOne(taskId);
        String projectId = oldTask.getProjectId();
        Task task = new Task();
        task.setName(name);
        task.setId(taskId);
        task.setProjectId(projectId);
        this.taskRepository.merge(task);
    }

    public void deleteTask(String taskId) {
        if (taskId == null || taskId.isEmpty() || this.taskRepository.findOne(taskId) == null) return;
        //про findOne ok?
        this.taskRepository.remove(taskId);
    }

    public void clearTasks(String projectId) {
        List<Task> list = this.taskRepository.findAll();
        for (Task task : list) {
            if (projectId.equals(task.getProjectId())) {
                this.taskRepository.remove(task.getId());
            }
        }
    }
}
