package ru.alekseev.tm.command.project;

import ru.alekseev.tm.bootstrap.Bootstrap;
import ru.alekseev.tm.command.system.AbstractCommand;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ProjectDeleteCommand extends AbstractCommand {
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public ProjectDeleteCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "delete-project";
    }

    @Override
    public String getDescription() {
        return "Delete project by id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DELETING OF PROJECT]");
        System.out.println("ENTER PROJECT ID");
        String projectId = reader.readLine();
        this.bootstrap.getProjectService().deleteProject(projectId);
        this.bootstrap.getTaskService().clearTasks(projectId);
        System.out.println("[OK. PROJECT AND IT'S TASKS DELETED]");
    }
}
