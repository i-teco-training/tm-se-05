package ru.alekseev.tm.command.project;

import ru.alekseev.tm.bootstrap.Bootstrap;
import ru.alekseev.tm.command.system.AbstractCommand;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ProjectUpdateCommand extends AbstractCommand {
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public ProjectUpdateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "update-project";
    }

    @Override
    public String getDescription() {
        return "Update project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATING OF PROJECT]");
        System.out.println("ENTER PROJECT ID");
        String projectId = reader.readLine();
        System.out.println("ENTER NEW NAME");
        String newName = reader.readLine();
        this.bootstrap.getProjectService().updateProject(projectId, newName);
        System.out.println("[OK]");
    }
}
