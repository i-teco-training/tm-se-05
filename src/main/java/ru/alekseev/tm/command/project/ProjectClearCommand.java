package ru.alekseev.tm.command.project;

import ru.alekseev.tm.bootstrap.Bootstrap;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.entity.Project;

import java.util.List;

public class ProjectClearCommand extends AbstractCommand {
    public ProjectClearCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "clear-projects";
    }

    @Override
    public String getDescription() {
        return "Delete all projects";
    }

    @Override
    public void execute() throws Exception {
        List<Project> list = this.bootstrap.getProjectService().showProjects();
        for (Project project : list) {
            this.bootstrap.getTaskService().clearTasks(project.getId());
        }
        this.bootstrap.getProjectService().clearProjects();
        System.out.println("[OK. ALL PROJECTS AND THEIR TASKS DELETED]");
    }
}
