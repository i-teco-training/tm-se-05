package ru.alekseev.tm.command.project;

import ru.alekseev.tm.bootstrap.Bootstrap;
import ru.alekseev.tm.command.system.AbstractCommand;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ProjectAddCommand extends AbstractCommand {
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public ProjectAddCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "add-project";
    }

    @Override
    public String getDescription() {
        return "Add new project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[ADDING OF NEW PROJECT]");
        System.out.println("ENTER NAME");
        String name = reader.readLine();
        this.bootstrap.getProjectService().addProject(name);
        System.out.println("[OK]");
    }
}
