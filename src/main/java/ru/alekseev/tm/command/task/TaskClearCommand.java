package ru.alekseev.tm.command.task;

import ru.alekseev.tm.bootstrap.Bootstrap;
import ru.alekseev.tm.command.system.AbstractCommand;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class TaskClearCommand extends AbstractCommand {
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public TaskClearCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "clear-tasks";
    }

    @Override
    public String getDescription() {
        return "Delete all tasks attached to project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CLEARING OF TASKS BY PROJECT]");
        System.out.println("ENTER PROJECT ID");
        String projectId = reader.readLine();
        this.bootstrap.getTaskService().clearTasks(projectId);
        System.out.println("[ALL TASKS ATTACHED TO PROJECT (projectId = " + projectId + ") DELETED]");
    }
}
