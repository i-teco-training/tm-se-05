package ru.alekseev.tm.command.task;

import ru.alekseev.tm.bootstrap.Bootstrap;
import ru.alekseev.tm.command.system.AbstractCommand;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class TaskDeleteCommand extends AbstractCommand {
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public TaskDeleteCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "delete-task";
    }

    @Override
    public String getDescription() {
        return "Delete task by id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DELETING OF TASK]");
        System.out.println("ENTER TASK ID");
        String taskId = reader.readLine();
        this.bootstrap.getTaskService().deleteTask(taskId);
        System.out.println("[OK]");
    }
}
