package ru.alekseev.tm.command.task;

import ru.alekseev.tm.bootstrap.Bootstrap;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.entity.Task;

import java.util.List;

public class TaskListCommand extends AbstractCommand {
    public TaskListCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "show-tasks";
    }

    @Override
    public String getDescription() {
        return "Show all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LIST OF ALL TASKS]");
        List<Task> list = this.bootstrap.getTaskService().showTasks();
        if (list.size() == 0) {
            System.out.println("LIST OF TASKS IS EMPTY");
        }
        for (int i = 0; i < list.size(); i++) {
            System.out.print(i + 1);
            System.out.println(") name:" + list.get(i).getName() + ", taskId: " + list.get(i).getId() + ", projectId: " + list.get(i).getProjectId());
        }
    }
}
