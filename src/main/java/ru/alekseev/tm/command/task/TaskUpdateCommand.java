package ru.alekseev.tm.command.task;

import ru.alekseev.tm.bootstrap.Bootstrap;
import ru.alekseev.tm.command.system.AbstractCommand;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class TaskUpdateCommand extends AbstractCommand {
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public TaskUpdateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "update-task";
    }

    @Override
    public String getDescription() {
        return "Update task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATING OF TASK]");
        System.out.println("ENTER TASK ID");
        String taskId = reader.readLine();
        System.out.println("ENTER NEW NAME");
        String newName = reader.readLine();
        this.bootstrap.getTaskService().updateTask(taskId, newName);
        System.out.println("[OK]");
    }
}
