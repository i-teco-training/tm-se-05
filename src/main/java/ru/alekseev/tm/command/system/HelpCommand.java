package ru.alekseev.tm.command.system;

import ru.alekseev.tm.bootstrap.Bootstrap;

import java.util.List;

public class HelpCommand extends AbstractCommand {
    public HelpCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Show all commands";
    }

    @Override
    public void execute() {
        List<AbstractCommand> allCommands = this.bootstrap.getCommands();

        for (AbstractCommand abstractCommand : allCommands) {
            System.out.println(abstractCommand.getName() + " - " + abstractCommand.getDescription());
        }
    }
}
