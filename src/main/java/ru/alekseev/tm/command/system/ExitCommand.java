package ru.alekseev.tm.command.system;

import ru.alekseev.tm.bootstrap.Bootstrap;

public class ExitCommand extends AbstractCommand {
    public ExitCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "Stop the Project Manager Application";
    }

    @Override
    public void execute() {
        System.exit(0);
    }
}
