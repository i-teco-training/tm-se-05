package ru.alekseev.tm.api;

import ru.alekseev.tm.service.ProjectService;
import ru.alekseev.tm.service.TaskService;

public interface ServiceLocator {

    ProjectService getProjectService();

    TaskService getTaskService();
}
